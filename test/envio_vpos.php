<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo de Envío de Datos V-POS - Integración V-POS</title>
    </head>
    <body>
        <?php 
            include("vpos_plugin.php");

            //Componentes de Seguridad
            //Vector Hexadecimal
            $vector = "ABCDEFAB0123BF00";

            //Llave Publica Crypto de Alignet
            $llaveVPOSCryptoPub = "-----BEGIN PUBLIC KEY-----\n".
            "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC0t0Cnbne8gQoeGK4nG6O3zfwh\n".
            "q8u9Wp5zHjyVYbvx2zudSOlBnJ5qU74BcTGypbn6W7jjvSNE7AmncOAVh4RxuRXO\n".
            "+bINFIyQ7/ErH/v1YpDFk8knC/NuvFpfHqhJ/5j2I8y+WmyF0MZmGtm074nUGv4d\n".
            "qlbUMT9aYUQ+RzMO7QIDAQAB\n".
            "-----END PUBLIC KEY-----";

            //Llave Firma Privada del Comercio
            $llaveComercioFirmaPriv = "-----BEGIN RSA PRIVATE KEY-----\n".
        "MIICXAIBAAKBgQDVgMvbGtcQywLlpRZkzHMUDgntytx/oP11OQqrZvjOkWWwEvXK\n".
        "i1sqU6tnFBfQ1ui3eoNTZ1VvdtCgKgQdKKtys/bXxcUYfoBRnBxi2xXvF7nOyptg\n".
        "JVLVCjk0Rgx1CAPqgh8DCidoB1jD24tneCUUzyWNVV8IPIcHrtuw6K4g/wIDAQAB\n".
        "AoGAf1lCv19kFvUwti/hiqBUqAqzq24MVeh+JlSu44wZLoWVhQ/aIkU7TKT5oRNg\n".
        "HUaDKyXdfXsuf7z8Nfy51XazxDD+npxcAMorwifTGwL5+c7UPWQIsYuFioUlJE0K\n".
        "TYlq2WMaFzi0J7ydtHY1XaQxLDrJvEL2nm/gdZDqZhnYH5ECQQD2kN7BaqTy6/fS\n".
        "OfvxiQ/5SHXPp0NTW/hh1QvvL1n03yuHfbd72e+x6e0/uNfoZ0IDuI1Q7d5Kr7Hf\n".
        "oshgXUDHAkEA3awS6vCbukg38nCE4RHTwF5eHou9fn4RkwZPXn020UKGWFqKH+nH\n".
        "76eiv8bThy7zmRJ2V+uwjxyea5u/5TxWCQJAYINCfmk1GdVd0w9ZXCkvdH91hgvN\n".
        "4bJNXlYbbsuVJbG5gzalfLhJB9YuRNQgx1qrz3MM9dG2QnvVX1mDn5zA/wJAFJht\n".
        "NYjfXyJgmFTd869dVi0uX3YqR/tclKVscGH/2tdsdf8LgEWPPvP7SggmFRRGq70s\n".
        "Y6TRRaqqCWNyI9FESQJBAIWLycybgdZtpRfDB79hDqaqmAtkT2HFu0UbVrp2y3IV\n".
        "JD7kHHHEmHnEVZgrgNMvKpn+Hi5qtr7Dt81iiRBCNhg=\n".
        "-----END RSA PRIVATE KEY-----";
            
            

            //Envío de Parametros a V-POS
            $array_send['acquirerId']='4';
            $array_send['commerceId']='7092';
            $array_send['purchaseOperationNumber']='10100163';
            //Monto incluido con impuestos
            $array_send['purchaseAmount']='10000';
            $array_send['purchaseCurrencyCode']='840';
            $array_send['commerceMallId']='1';
            $array_send['language']='SP';            
            $array_send['billingFirstName']='Juan';
            $array_send['billingLastName']='Perez';
            $array_send['billingEMail']='test@test.com';
            $array_send['billingAddress']='Direccion ABC';
            $array_send['billingZIP']='1234567890';
            $array_send['billingCity']='Montevideo';
            $array_send['billingState']='Montevideo';
            $array_send['billingCountry']='UY';
            $array_send['billingPhone']='123456789';
            $array_send['shippingAddress']='Direccion ABC';
            $array_send['terminalCode']='VBV00663';
            
            //Ejemplo envío campos reservados en parametro reserved1.
            $array_send['reserved1']='Valor Reservado 123';

            $array_send['reserved10']='6';
            $array_send['reserved11']='1111111';
            //Monto Gravado: Monto de la transacción usado para calcular el monto de IVA.
            echo "Purchase Amount: ".$array_send['purchaseAmount']."<br/>";
            echo "Purchase Amount con decimales: ".$array_send['purchaseAmount'] / 100 ."<br/>";
            echo "Reservado 12: ". $array_send['purchaseAmount'] / 100 / (1 + 0.10)."<br/>";
            echo "Reservado 12 Solo 2 decimales: ". round($array_send['purchaseAmount'] / 100 / (1 + 0.10),2)."<br/>";
            echo "Reservado 12 Solo 2 decimales Sin '.': ". str_replace('.','',round($array_send['purchaseAmount'] / 100 / (1 + 0.10),2))."<br/>";
            $array_send['reserved12'] = str_replace('.','',round($array_send['purchaseAmount'] / 100 / (1 + 0.10),2));            
            //$array_send['reserved12']=$array_send['purchaseAmount'];    


            //Parametros de Solicitud de Autorización a Enviar
            $array_get['XMLREQ']="";
            $array_get['DIGITALSIGN']="";
            $array_get['SESSIONKEY']="";

            //Ejecución de Creación de Valores para la Solicitud de Autorización
            VPOSSend($array_send,$array_get,$llaveVPOSCryptoPub,$llaveComercioFirmaPriv,$vector);
        ?>

        <form name="frmVPOS" method="POST" action="https://vpayment.verifika.com/VPOS/MM/transactionStart20.do">
            <INPUT TYPE="text" NAME="IDACQUIRER" value="<?php echo $array_send['acquirerId'];?>">
            <INPUT TYPE="text" NAME="IDCOMMERCE" value="<?php echo $array_send['commerceId'];?>">
            <INPUT TYPE="text" NAME="XMLREQ" value="<?php echo $array_get['XMLREQ'];?>">
            <INPUT TYPE="text" NAME="DIGITALSIGN" value="<?php echo $array_get['DIGITALSIGN'];?>">
            <INPUT TYPE="text" NAME="SESSIONKEY" value="<?php echo $array_get['SESSIONKEY'];?>">
            <input type="submit" text="Comprar"/>
        </form>
    </body>
</html>