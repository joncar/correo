<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo de Recepción de Datos V-POS - Integración V-POS</title>
    </head>
    <body>
        <?php 
            include("vpos_plugin.php");

            //Componentes de Seguridad
            //Vector Hexadecimal
            $vector = "ABCDEFAB0123BF00";

            //Llave Firma Publica de Alignet
            $llaveVPOSFirmaPub = "-----BEGIN PUBLIC KEY-----\n".
            "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCtvXnikeSS+H/Qs/51iL3ZYPfz\n".
            "KW94WUAz7IdZIOIcuG1zLIR3kUNUc/vdSmW120dwkIleB6pl4cVT5nDewBFJCzTS\n".
            "W6jGaWaryzl7xS3ZToKTHpVeQr3avN7H+Om9TfsccY7gBV3IOIauTg9xIpDjIg52\n".
            "fUcfyPq+Bhw0cWkDUQIDAQAB\n".
            "-----END PUBLIC KEY-----";

            //Llave Crypto Privada del Comercio
            $llaveComercioCryptoPriv = "-----BEGIN RSA PRIVATE KEY-----\n".
            "MIICXQIBAAKBgQChkt0P012gPFuAs+pT1ZPv9Iet0t5jkCtyJnpfoRF/rpsBvNnP\n".
            "ecULTfFgksBIfNRk5iSCKwvS2x2YWeSAyWAnrRBWal+bHTP7Ghpm9JyMeCBtqE7+\n".
            "Wky89SUlIhVL0G8822a9IMKuSrQLZKQHUdvoBWKGxiimNtS0xqRA2k9LkwIDAQAB\n".
            "AoGALfCeHkPQ2SCQO9XsCI4vlAiJE2ljhheV4s0B+nLHkPBKboINxCoxVneA7B6N\n".
            "wY6MrWuqccBfikJ29byJdfzL88hIquAw/pB1RwXnfoo51R7+iMa7LV3DhBgmHNqn\n".
            "Z8k/4Oe4GkRBtb7en3h0SYSOYiniRQZv37R3MNATp0zoppECQQDTmGbDXiyjIofl\n".
            "sawacGQ9kjaknCn8Rz2ATmMYFay9qmNv63MdprsFkSLpgbAidrydStEIFt6ImhIR\n".
            "7ETQ5PE/AkEAw3sijgf16rJ4aLexEwtGzIQpXl9FjuPsYONUGCxD0Hjt3jXUDMgC\n".
            "Ub46nxOyJhT7l/N0qwQKK1921fWDaBK8rQJAIPbjO5iWV6vvhAdUCqJr23PF84so\n".
            "t1bZ6/1KTtxBlyLObwc9Xec1x74+tCYyLIxXAiI/woTdkmZ+XjBy4CBqvQJBALJI\n".
            "TFSTKBKcILsIxWOkBjciVgRvCaZvczdOPXUqcdLhZ7ghCbt6crsQrrBEq1aWDnwg\n".
            "GwiZz5iNIXmzx9wUMqUCQQCUn5XnaJpj/Hu/PYeaquvkVchai4buf50wqaP02U7q\n".
            "27AgVQaabXSl5E4+LtfqVcn8fTxijDP4kVdDWtHcCf3E\n".
            "-----END RSA PRIVATE KEY-----";

            //Parametros de Recepción de Autorización
            $arrayIn['IDACQUIRER'] = $_POST['IDACQUIRER'];
            $arrayIn['IDCOMMERCE'] = $_POST['IDCOMMERCE'];
            $arrayIn['XMLRES'] = $_POST['XMLRES'];
            $arrayIn['DIGITALSIGN'] = $_POST['DIGITALSIGN'];
            $arrayIn['SESSIONKEY'] = $_POST['SESSIONKEY'];
            $arrayOut = '';
                
            //Ejecución de Creación de Valores para la Solicitud de Interpretación de la Respuesta
            if(VPOSResponse($arrayIn,$arrayOut,$llaveVPOSFirmaPub,$llaveComercioCryptoPriv,$vector)){                
            }else{
                echo "Error durante el proceso de interpretación de la respuesta. "
                . "Verificar los componentes de seguridad: Vector Hexadecimal y Llaves.";
            }
        ?>
        
        <table>
            <tr><td>Resultado de la Transacción</td><td><?php echo $arrayOut['authorizationResult'];?></td></tr>
            <tr><td>Detalle del Resultado</td><td><?php echo $arrayOut['errorCode'] . " - " . $arrayOut['errorMessage'];?></td></tr>                        
            <tr><td>Número de Operacion</td><td><?php echo $arrayOut['purchaseOperationNumber'];?></td></tr>
            <tr><td>Monto</td><td><?php echo "$. " . $arrayOut['purchaseAmount']/100 .".00";?></td></tr>
            <!--Ejemplo recepción de campos reservados en parametro reserved1-->
            <tr><td>Reservado 1</td><td><?php echo $arrayOut['reserved1'];;?></td></tr>
            <!--Ejemplo recepción de campos reservados de la inclusión financiera-->
            <tr><td>Reservado 13</td><td><?php echo $arrayOut['reserved13'];;?></td></tr>
            <tr><td>Reservado 14</td><td><?php echo $arrayOut['reserved14'];;?></td></tr>
            <tr><td>Reservado 15</td><td><?php echo $arrayOut['reserved15'];;?></td></tr>
            <tr><td>Reservado 16</td><td><?php echo $arrayOut['reserved16'];;?></td></tr>
            <tr><td>Reservado 17</td><td><?php echo $arrayOut['reserved17'];;?></td></tr>
	</table>
    </body>
</html>