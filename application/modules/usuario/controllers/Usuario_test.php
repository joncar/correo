<?php 
/**************************
 * Archivo de prueba de alignet.
 */
    require_once APPPATH.'controllers/Panel.php';    
    class Usuario extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function getAlignetArray($venta){
            $this->load->helper('alignet');

            //Componentes de Seguridad
            //Vector Hexadecimal
            $vector = "0FF00AA000000000";
            /*$vector = "ABCDEFAB0123BF00"; //Produccion*/

            //Llave Publica Crypto de Alignet
            $llaveVPOSCryptoPub = "-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDTJt+hUZiShEKFfs7DShsXCkoq
TEjv0SFkTM04qHyHFU90Da8Ep1F0gI2SFpCkLmQtsXKOrLrQTF0100dL/gDQlLt0
Ut8kM/PRLEM5thMPqtPq6G1GTjqmcsPzUUL18+tYwN3xFi4XBog4Hdv0ml1SRkVO
DRr1jPeilfsiFwiO8wIDAQAB
-----END PUBLIC KEY-----";

            //Llave Firma Privada del Comercio
            $llaveComercioFirmaPriv = "-----BEGIN RSA PRIVATE KEY-----
MIICWwIBAAKBgQCoLR283T4awqR4OiA6pj+cetc+LJgJTekU7MFAZMVwTadUKoMg
/N3hRpSeVLG/79M7Utnw6+ZlMLryXHlgyZZSnkpfzqO+8eTkLb2N/qq/73gqFenY
0KDjYS53yaGfOtRitZ2dXAmJNI+Kfap7w/HuTzawfmjZ2QckhYmel9b4WQIDAQAB
AoGAL7zWwySeG2SC4SnGuj80sUthS+HGqmV2Mx1dYdT5An7Yb5i9riB5Lxgk2ai6
5/vAnwdOnAeqV7D0cBV10taGF6mZneGRXU21NieMuUVC66wAd4C/e3QgEOrRTSxH
ynIr/5aLGtc7irZ5NLoE1cUi3Okarhu30Lh6icZs84LW+E0CQQDccKOao4GGGZRd
tpVfIwkU2iUf4JuchTUbzDd2bV8YNpPPDDFOyp3xh13MEzSZd0vkmxntwIEXzhW+
gP7gbmujAkEAw04tDm3usS9YDxGuDSK4ugBSlc3XxmWCVULgNWlb3VYaDpcEgfaT
tnZRD8BRHZ4KmmRxqm701Jvfocyx1D7L0wJAE45SlUdoGDNwvBeUjMoY5iasIHXW
mD4hBgRJ71GonAdLeZTFUw4akPDHW3FguzSgIPm5hSQMz8XSnAHpmpNxLQJAIFb9
lvoVIDnatwQSchJhzErpYezn4e5FGqEsRyvfDlOHlQrksDCIXRpSae1KgUD+OHqR
Z8tftyTobv5aI+8SCQJAO528SmFGtQDPVwaTa6erFvUmTRAiKNVToNnWtP62R/4v
4vNLgrG9/GZaw1P8ZmvlrVfOXKaO886RJSOGYgURSg==
-----END RSA PRIVATE KEY-----";

            //Envío de Parametros a V-POS
            $array_send['acquirerId']='16';
            // Produccion $array_send['commerceId']='7092';
            $array_send['commerceId']='8020';
            $array_send['purchaseOperationNumber']=$venta->id.date("dhis");
            //Monto incluido con impuestos
            $array_send['purchaseAmount']= ($venta->precio*100);
            $array_send['purchaseCurrencyCode']='840';//858 peso uruguayos, 840 dolares
            $array_send['commerceMallId']='1';
            $array_send['language']='SP';            
            $array_send['billingFirstName']=$this->user->nombre;
            $array_send['billingLastName']=$this->user->apellido;
            $array_send['billingEMail']=$this->user->email;
            $array_send['billingAddress']=$this->user->direccion;
            $array_send['billingZIP']=$this->user->zip;
            $array_send['billingCity']=$this->user->ciudad;
            $array_send['billingState']=$this->user->estado;
            $array_send['billingCountry']='UY';
            $array_send['billingPhone']=$this->user->telefono;
            $array_send['shippingAddress']=$this->user->direccion;
            $array_send['terminalCode']='00000000';
            
            //Ejemplo envío campos reservados en parametro reserved1.
            $array_send['reserved1']='Valor Reservado 123';

            //Parametros Reservados Sobre Inclusión Financiera
            $array_send['reserved10']='6'; //6
            $array_send['reserved11']='1111111'; //1111111
            //Monto Gravado: Monto de la transacción usado para calcular el monto de IVA.
            //$array_send['reserved12'] = str_replace('.','',round($array_send['purchaseAmount'] / 100 / (1 + 0.10),2));            
            $array_send['reserved12'] = ($venta->precio*100);

            //Parametros de Solicitud de Autorización a Enviar
            $array_get['XMLREQ']="";
            $array_get['DIGITALSIGN']="";
            $array_get['SESSIONKEY']="";
            $this->db->update('envios',array('process_id'=>$array_send['purchaseOperationNumber']),array('id'=>$venta->id));
            //Ejecución de Creación de Valores para la Solicitud de Autorización
            VPOSSend($array_send,$array_get,$llaveVPOSCryptoPub,$llaveComercioFirmaPriv,$vector);
            return array($array_send,$array_get);
        }
        
        function pagar($id = '',$idc = '',$tipo = ''){
            if(is_numeric($id)){
                $venta = $this->db->get_where('envios',array('id'=>$id));
                if(empty($tipo)){
                    $this->loadView(array('view'=>'seleccionarpago','alignet'=>$this->getAlignetArray($venta->row()),'id'=>$id,'title'=>'Cargar información de pago','venta'=>$venta->row()));
                }else{
                    switch($tipo){
                       case 'abitat':
                            if(!empty($_POST)){
                                $this->form_validation->set_rules('descripcion_pago','Descripcion de pago','required');
                                $this->form_validation->set_rules('process_id','#Confirmaciòn','required');                                
                                if($this->form_validation->run()){                                    
                                    $this->db->update('envios',array('tipo_pago'=>1,'process_id'=>$_POST['process_id'],'descripcion_pago'=>$_POST['descripcion_pago']),array('id'=>$id));
                                    $this->loadView(array('view'=>'seleccionarpago','id'=>$id,'success'=>1,'title'=>'Cargar información de pago'));
                                }else{
                                    $this->loadView(array('view'=>'seleccionarpago','id'=>$id,'error'=>$this->form_validation->error_string(),'title'=>'Cargar información de pago','venta'=>$venta->row()));
                                }
                            }
                        break;
                        case 'transferencia':
                            if(!empty($_POST)){
                                $this->form_validation->set_rules('descripcion_pago','Descripcion de pago','required');
                                $this->form_validation->set_rules('process_id','#Confirmaciòn','required');
                                $this->form_validation->set_rules('banco','Banco','required');
                                if($this->form_validation->run()){
                                    $_POST['descripcion_pago'].= ' en el banco '.$_POST['banco'];
                                    $this->db->update('envios',array('tipo_pago'=>2,'process_id'=>$_POST['process_id'],'descripcion_pago'=>$_POST['descripcion_pago']),array('id'=>$id));
                                    $this->loadView(array('view'=>'seleccionarpago','id'=>$id,'success'=>1,'title'=>'Cargar información de pago'));
                                }else{
                                    $this->loadView(array('view'=>'seleccionarpago','id'=>$id,'error'=>$this->form_validation->error_string(),'title'=>'Cargar información de pago','venta'=>$venta->row()));
                                }
                            }
                        break;
                        default:
                            echo 'Ir a la api';
                        break;
                    }
                }
            }else{
                if($id=='success'){
                    $this->db->join('user','user.id = envios.user_id');
                    $venta = $this->db->get_where('envios',array('process_id'=>$idc));
                    if($venta->num_rows>0){
                        $this->loadView(array('view'=>$id.'_payment','id'=>$idc,'venta'=>$venta->row()));
                    }                        
                }else{
                    $this->loadView(array('view'=>'fail_payment','id'=>$idc));
                }
            }                
            
        }
        
        function reversar($id,$idc = ''){
            if(is_numeric($id)){
                $this->load->library('bancard');
                $venta = $this->db->get_where('ventas',array('id'=>$id));
                if($venta->num_rows>0){                    
                    if($this->bancard->rollback($id,$venta->row()->total)){
                        $this->loadView(array('view'=>'success_payment','id'=>$idc));
                    }else{
                        $this->loadView(array('view'=>'fail_payment','id'=>$idc));
                    }
                }
            }else{
                $this->loadView(array('view'=>'fail_payment','id'=>$idc));
            }
        }
        
        function get_confirm($id,$idc = ''){
            if(is_numeric($id)){
                $this->load->library('bancard');
                $venta = $this->db->get_where('ventas',array('id'=>$id));
                if($venta->num_rows>0){                    
                   $this->bancard->get_confirm($id,$venta->row()->total);
                }
            }
        }
    }