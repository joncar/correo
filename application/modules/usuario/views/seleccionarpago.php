<div class="container" style="margin-top:30px;">
    <?php if(empty($success)): ?>
    <?php if(!empty($error)): ?>
        <div class="alert alert-danger"><?= $error ?></div>
    <?php endif ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title">
                Selecciona un tipo de pago
            </h1>
        </div>
        <div class="panel-body">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-credit-card"></i> ABITAB</a></li>                        
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-bank"></i> TRANSFERENCIA</a></li>
                        <li role="presentation"><a href="#visa" aria-controls="visa" role="tab" data-toggle="tab"><i class="fa fa-credit-card"></i> VISA</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class='row'>
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-4">                                        
                                           <div class='well'>                                                                                                   
                                                    <?= $this->db->get('ajustes')->row()->descripcion_pago_abitat ?>
                                               <div class='row' style="margin-top:30px">
                                                <div class='col-xs-12 col-sm-12'>
                                                    <ul class='list-group'>                                                        
                                                        <li class="list-group-item"><b>Importe: </b>  $<?= $venta->precio ?></li>
                                                    </ul>
                                                </div>
                                               </div>
                                           </div>
                                           <form method="post" action="<?= base_url('usuario/pagar/'.$id.'/'.$id.'/abitat') ?>">
                                                   <div class="form-group">
                                                     <label for="exampleInputEmail1">Descripción de pago</label>
                                                     <input type="text" class="form-control" name="descripcion_pago" value='Pago realizado por abitab' readonly="">
                                                   </div>
                                                   <div class="form-group">
                                                     <label for="exampleInputEmail1">Nro de de confirmación:</label>
                                                     <input type="text" class="form-control" placeholder="#Confirmación" name='process_id'>
                                                   </div>
                                                   <button type="submit" class="btn btn-success btn-block"><i class="fa fa-bank"></i> Seleccionar Pago</button>
                                           </form>
                                    </div>                                
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="messages">
                            <div class='row'>
                                    <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                                           <div class='well'>
                                               <?php foreach($this->db->get('bancos')->result() as $b): ?>
                                                    <div class='row'>
                                                        <div class='col-xs-12 col-sm-6'>
                                                            <img src='<?= base_url('img/bancos/'.$b->logo) ?>' style="width:100%">
                                                        </div>
                                                    </div>
                                                    <div class='row' style="margin-top:30px">
                                                         <div class='col-xs-12 col-sm-12'>
                                                             <ul class='list-group'>
                                                                 <li class="list-group-item"><b>Banco: </b> <?= $b->bancos_nombre ?></li>
                                                                 <li class="list-group-item"><b>#Cuenta: </b>  <?= $b->nro_cuenta ?></li>                                                             
                                                                 <li class="list-group-item"><b>Tipo de cuenta: </b>  <?= $b->tipo_cuenta ?></li>
                                                                 <li class="list-group-item"><b>Importe: </b>  $<?= $venta->precio ?></li>
                                                             </ul>
                                                         </div>
                                                    </div>
                                               <?php endforeach ?>
                                           </div>
                                           <form method="post" action="<?= base_url('usuario/pagar/'.$id.'/'.$id.'/transferencia') ?>">
                                                   <div class="form-group">
                                                     <label for="exampleInputEmail1">Descripción de pago</label>
                                                     <input type="text" class="form-control" name="descripcion_pago" value='Pago realizado por transferencia bancaria' readonly="">
                                                   </div>
                                                   <div class="form-group">
                                                     <label for="exampleInputEmail1">Banco:</label>
                                                     <?php
                                                        echo form_dropdown_from_query('banco','bancos','bancos_nombre','bancos_nombre');
                                                     ?>
                                                   </div>
                                                   <div class="form-group">
                                                     <label for="exampleInputEmail1">Nro de de confirmación:</label>
                                                     <input type="text" class="form-control" placeholder="#Confirmación" name='process_id'>
                                                   </div>
                                                   <button type="submit" class="btn btn-success btn-block"><i class="fa fa-bank"></i> Seleccionar Pago</button>
                                           </form>
                                    </div>
                           </div>
                        </div>
                    
                        <div role="tabpanel" class="tab-pane" id="visa">
                            <div class='row'>
                                <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                                    <div class='well'>                                                                                                   
                                       <img src="<?= base_url('images/alignet.jpg') ?>" style="width:100%">                                       
                                       <div class="row">
                                            <div class='col-xs-12 col-sm-12'>
                                                <ul class='list-group'>                                                        
                                                    <li class="list-group-item"><b>Importe: </b>  $<?= $venta->precio ?></li>
                                                </ul>
                                            </div>
                                           <div class="col-xs-12">
                                               <!--<form name="frmSolicitudPago" method="POST" action="https://integracion.alignetsac.com/VPOS/MM/transactionStart20.do">-->
                                               <form name="frmSolicitudPago" method="POST" action="https://vpayment.verifika.com/VPOS/MM/transactionStart20.do">
                                                    <input TYPE="hidden" name="IDACQUIRER" value="<?= $alignet[0]['acquirerId'] ?>">
                                                    <input TYPE="hidden" name="IDCOMMERCE" value="<?= $alignet[0]['commerceId'] ?>">
                                                    <input TYPE="hidden" name="XMLREQ" value="<?= $alignet[1]['XMLREQ'] ?> ">
                                                    <input TYPE="hidden" name="DIGITALSIGN" value="<?= $alignet[1]['DIGITALSIGN'] ?>">
                                                    <input TYPE="hidden" name="SESSIONKEY" value="<?= $alignet[1]['SESSIONKEY'] ?>">
                                                    <button type="submit" class="btn btn-info btn-block">Cargar Pago</button>
                                               </form>                                               
                                           </div>
                                      </div>
                                   </div>
                               </div>
                           </div>
                        </div>
                </div>
              </div>
        </div>
    </div>
    <?php else: ?>
    <div class='alert alert-success'>
        Sus datos han sido enviados con èxito, validaremos su informaciòn y le será notificado cuando su pago sea procesado. 
    </div>
        <ul class="list-group">
            <li class='list-group-item'><a href="<?= base_url('panel') ?>">Ir a mis envios</a></li>            
        </ul>

    <?php endif; ?>         
</div><!-- /.row -->