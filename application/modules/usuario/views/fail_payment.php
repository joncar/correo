<div class="panel panel-danger">
    <div class="panel-body">
        <div style="text-align: center; margin-top:30px;">
            <i class="fa fa-times-circle-o fa-5x" style="color:red"></i>
        </div>
        <h2 style="text-align: center;">
            Se ha cancelado el pago de la factura  #<?= $id ?> 
        </h2>
        <p align="center"><a class="btn btn-info" href="<?= base_url('usuario/compras') ?>">Ir a mis compras</a></p>
        <p align="center"><a class="btn btn-info" href="<?= base_url() ?>">Seguir comprando</a></p>
    </div>
</div>