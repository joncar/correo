<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function ajustes(){
            $crud = $this->crud_function('','');
            $crud->unset_add()->unset_delete();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function bancos(){
            $crud = $this->crud_function('','');
            $crud->field_type('tipo_cuenta','enum',array('Ahorro','Corriente'));
            $crud->set_field_upload('logo','img/bancos');
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
