<div class="section-area wow" data-sr-id="1" style="; visibility: visible; transform: translateY(0) scale(1); opacity: 1;-webkit-transition: -webkit-transform 1s cubic-bezier( 0.6, 0.2, 0.1, 1 ) 0s, opacity 1s cubic-bezier( 0.6, 0.2, 0.1, 1 ) 0s; transition: transform 1s cubic-bezier( 0.6, 0.2, 0.1, 1 ) 0s, opacity 1s cubic-bezier( 0.6, 0.2, 0.1, 1 ) 0s; ">
    <div class="container">
        <div class="row">
            <h1 data-editable="" data-name="ed0"></h1>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="section-default border-top" data-editable="" data-name="content1"><h1 data-name="ed1">
    Costo de transporte desde Ciudad del Este Paraguay a Uruguay
</h1>
<p data-name="ed2">
    La suscripción en mvdeocorreos.com, es totalmente gratis.
</p>
<p data-name="ed3">
    Somos el nexo perfecto entre Paraguay y Uruguay. Nos enfocamos en brindar una solución global, lo que nos permite tener total control sobre los procesos.
</p>
<p data-name="ed4">
    Un mismo equipo en ambos países, con la meta de ofrecer un servicio de excelente calidad y hacer del transporte de cargas algo sencillo, global y con tarifas competitivas.
</p>
<p>
    Cargo por manejo (Handling)
</p>
<p>
    Aplica un cargo de USD 5 de manejo por envío a Uruguay (único o consolidado).
</p>
<h1>
    Envíos al interior del país
</h1>
<p>
    Los envíos al interior del país tienen un costo adicional de USD 5 el primer Kg, y USD 0,50 por Kg adicional. En convenio con DAC grupo Agencia.
</p>
<p>
    * Las tarifas incluyen TSPU (Tasa de Servicio Postal Universal)
</p></div>
            </div>
            <div class="col-xs-6">
                <div class="section-default border-top" data-editable="" data-name="content2"><h1 data-name="ed5">
    Precios
</h1>
<table class="table table-borded">
    <thead>
        <tr>
            <th>
                Desde
            </th>
            <th>
                Hasta
            </th>
            <th>
                Precio
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                1 Gramo
            </td>
            <td>
                899 Gramos
            </td>
            <td>
                USD 18
            </td>
        </tr>
        <tr>
            <td>
                0,9 kilos
            </td>
            <td>
                5
            </td>
            <td>
                USD 20 x kilo
            </td>
        </tr>
        <tr>
            <td>
                5
            </td>
            <td>
                20
            </td>
            <td>
                USD 15 x kilo
            </td>
        </tr>
        <tr>
            <td>
                20
            </td>
            <td>
                40
            </td>
            <td>
                USD 12 &nbsp;x kilo
            </td>
        </tr>
        <tr>
            <td>
                40
            </td>
            <td>
                o más
            </td>
            <td>
                Cotizar precio
            </td>
        </tr>
    </tbody>
</table></div>
            </div>
        </div>
    </div><!-- /section-area -->
</div>            