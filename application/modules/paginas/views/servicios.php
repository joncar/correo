
        
        <div class="section-area wow" data-sr-id="1" style="; visibility: visible; transform: translateY(0) scale(1); opacity: 1;-webkit-transition: -webkit-transform 1s cubic-bezier( 0.6, 0.2, 0.1, 1 ) 0s, opacity 1s cubic-bezier( 0.6, 0.2, 0.1, 1 ) 0s; transition: transform 1s cubic-bezier( 0.6, 0.2, 0.1, 1 ) 0s, opacity 1s cubic-bezier( 0.6, 0.2, 0.1, 1 ) 0s; ">
    <div class="container">
        <div class="row">
            <h1 data-editable="" data-name="ed0"><p class="text-left" data-name="ed1">
    <b>Nuestros Servicios</b>
</p></h1> 
       <h2>
       <i><b>Comprar On-line</b></i>
      </h2>
<p data-name="ed2">
    Simplemente utiliza tu número de usuario asignado para registrarte en el sitio tiendaparaguay.com, para que te envíen tus compras.
</p>
<h2>
    <b><i>Paquetes</i></b>
</h2>
<p data-name="ed3">
    Gracias a la franquicia existente en el Uruguay, recuerda que cada persona física tiene hasta 4 envíos por año de hasta 200 USD de valor en factura exonerados de todo impuesto por Aduanas.
</p>
<h2>
    <b><i>Notificaciones</i></b>
</h2>
<p data-name="ed4">
    ¡Estarás al tanto de todo! Una vez que recibamos tu envío en nuestro depósito de Ciudad del Este, Paraguay, te enviaremos un E-mail para notificarte.
</p>
<h2>
    <b><i>Entregas puerta a puerta</i></b>
</h2>
<p data-name="ed5">
    Nos encargamos de transportar tu envío a Uruguay y que llegue a tus manos en un plazo de 7-10 días hábiles ¡sin complicaciones!
</p>
<h2>
    <b><i>Consolidación de cargas</i></b>
</h2>
<p data-name="ed6">
    Sabemos lo importante que es la consolidación para garantizar un costo menor en el corte de guía, gastos y trámites aduaneros. Es por ello que le ofrecemos la posibilidad de recibir su mercadería en nuestro depósito, almacenarla, y transportarla a Uruguay en base a sus indicaciones.
</p>
<p style="”text-align:" justify;="" data-name="ed7">
    Este servicio le permite manejar su stock e inventario de manera online para luego ser transportada de la forma que le sea más conveniente. Si lo prefieren todas sus compras en un solo paquete. Recuerda que la consolidación aplica únicamente para paquetes que vayan a ingresar dentro de la franquicia.
</p>
<h2>
    <b><i>Transporte Internacional</i></b>
</h2>
<p data-name="ed8">
    Nuestro servicio esencial de transporte de mercadería, compras, encomiendas o correspondencia en nuestro vuelo semanal.
</p>
<p data-name="ed9">
    El proceso es muy sencillo, todo lo que se recibe durante la semana se procesa en nuestra oficina en Ciudad del Este, y se envía a Montevideo. Te mantenemos informado sobre el estado de tu paquete en todo momento, y si lo deseas puedes hacer un seguimiento personalizado.
</p>
<p data-name="ed10">
    La tarifa del flete es la misma sin importar el contenido de tu envío.
</p>
<h2>
    <b><i>Transporte de Cargas</i></b>
</h2>
<p data-name="ed11">
    Somos el nexo perfecto entre Paraguay y Uruguay. Nos enfocamos en brindar una solución global, lo que nos permite tener total control sobre los procesos.
</p>
<p style="”text-align:" justify;="" data-name="ed12">
    Un mismo equipo en ambos países, con la meta de ofrecer un servicio de excelente calidad y hacer del transporte de cargas algo sencillo, global y con tarifas competitivas.
</p>
<h2><b><i>Envíos al interior del país</i></b></h2>
            <p data-name="ed13"> Los envíos al interior del país tienen un costo adicional de USD 5 el primer Kg, y USD 0,50 por Kg adicional. En convenio con DAC grupo Agencia.</p>

<p data-name="ed14">Las tarifas incluyen TSPU (Tasa de Servicio Postal Universal)</p>
 <h2><b><i>Retiro Oficina Montevideo – Uruguay </i></b></h2>
   <p data-name="ed15">Para tu comodidad te ofrecemos también pasar por nuestra oficina y retirar tu envío, nuestra ubicación (mapa google) con estacionamiento siempre en la puerta y alrededores.</p>

<h2><b><i>Despacho e Importaciones </i></b></h2>
            <p data-name="ed16">Si tu paquete al llegar a Uruguay no cumple con los requisitos para ser liberado bajo la franquicia, te ofrecemos un servicio integral con el cual Mvdeocorreos se encarga del proceso de despacho e importación de tu mercadería.</p>





<p data-name="ed17">
    
</p>
        </div>
          <div class="row">
            <div class="col-xs-12">
                <div class="section-default border-top" data-editable="" data-name="content1"></div>
            </div>
        </div>
        
    </div><!-- /section-area -->
</div>                