<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function envios(){
            $crud = $this->crud_function('','');
            $crud->set_relation('user_id','user','{id}|{nombre}|{apellido}');
            $crud->columns('id','je8701ad4.id','je8701ad4.nombre','je8701ad4.apellido','descripcion','area','peso','valor','fecha','status');
            $crud->display_as('je8701ad4.nombre','Nombre')
                     ->display_as('je8701ad4.apellido','Apellido')
                     ->display_as('je8701ad4.id','ID usuario')
                     ->display_as('id','#Tracking')
                     ->display_as('process_id','#Confirmación');
            $crud->field_type('tipo_pago','dropdown',array('1'=>'ABITAB','2'=>'Transferencia Bancaria','3'=>'Pago en Comercio','4'=>'Pago con tarjeta de crédito'));
            $crud->callback_column('je8701ad4.id',function($val,$row){
                return explode('|',$row->se8701ad4)[0];
            });
            $crud->callback_column('je8701ad4.nombre',function($val,$row){
                return explode('|',$row->se8701ad4)[1];
            });
            $crud->callback_column('je8701ad4.apellido',function($val,$row){
                return explode('|',$row->se8701ad4)[2];
            });
            $crud->add_action('<i class="fa fa-money"></i> Cargar Pago','',base_url('envios/admin/pagos/add').'/');
            $crud->add_action('<i class="fa fa-print"></i> Factura HTML','',base_url('reportes/admin/verReportes/4/html/id/').'/');
            $crud->add_action('<i class="fa fa-print"></i> Factura PDF','',base_url('reportes/admin/verReportes/4/pdf/id/').'/');
            $crud->add_action('<i class="fa fa-print"></i> Etiqueta','',base_url('reportes/admin/verReportes/10/html/id/').'/');
            $crud->field_type('status','set',array('Recibido','Embalado','Embarcado','Recibido en aduana','Distribución','Disponible','Pagado','Entregado'));
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function pagos($x = '',$y = ''){
            if($x=='add' && is_numeric($y)){
                redirect(base_url('envios/admin/pagos/'.$y.'/add'));
            }
            $crud = $this->crud_function('','');
            if(is_numeric($x)){
                $pedido = $this->db->get_where('envios',array('id'=>$x));
                if($pedido->num_rows()>0){
                    $crud->field_type('user_id','hidden',$pedido->row()->user_id);
                    $crud->field_type('envios_id','hidden',$x);
                }
            }            
            $crud->field_type('status','dropdown',array(
                '-1'=>'No procesado',
                 '1'=>'Por procesar',
                 '2'=>'Procesado',
            ));
            $crud->callback_column('procesado',function($val,$row){
                switch($val){
                    case '-1': return '<span class="label label-danger">No procesado</span>'; break;
                    case '1': return '<a href="'.site_url('usuario/comprar/'.$row->id).'" class="label label-default">Por procesar</a>'; break;
                    case '2': return '<span class="label label-success">Procesado</span>'; break;
                }
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
    }
?>
