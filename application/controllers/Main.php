<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        date_default_timezone_set('America/Asuncion');
    }

    public function index() {
        $this->loadView('main');
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']))
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    } else
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url('panel'));
        } else
            header("Location:" . base_url('panel'));
    }

    function pages($titulo) {
        $titulo = urldecode(str_replace("-", "+", $titulo));
        if (!empty($titulo)) {
            $pagina = $this->db->get_where('paginas', array('titulo' => $titulo));
            if ($pagina->num_rows() > 0) {
                $this->loadView(array('view' => 'paginas', 'contenido' => $pagina->row()->contenido, 'title' => $titulo));
            } else
                $this->loadView('404');
        }
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    public function loadView($param = array('view' => 'main')) {
        if (is_string($param))
            $param = array('view' => $param);
        $this->load->view('template', $param);
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }

    function sendMailer() {
        $this->load->library('mailer');
        $this->mailer->mail('joncar.c@gmail.com', 'Test', 'Test');
    }

    function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
            foreach($usuario as $n=>$v){             
             $mensaje->texto = str_replace('{usuarios.'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{usuarios.'.$n.'}',$v,$mensaje->titulo);   
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }
            //correo('joncar.c@gmail.com',$mensaje->titulo,$mensaje->texto);
        }
        
        function testCorreo(){
            /*$this->enviarcorreo((object)array('email'=>'joncar.c@gmail.com','nombre'=>'Jonathan'),1);
            $this->enviarcorreo((object)array('email'=>'pedromarecos23@hotmail.com','nombre'=>'Jonathan'),1);*/
            $this->enviarcorreo((object)array('email'=>'xxx@mail-tester.com','nombre'=>'Jonathan'),1);
        }
        
        function procesar_pago(){
            $this->load->helper('alignet');
            //Componentes de Seguridad
            //Vector Hexadecimal
            $vector = "ABCDEFAB0123BF00";

            //Llave Firma Publica de Alignet
            $llaveVPOSFirmaPub = "-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCtvXnikeSS+H/Qs/51iL3ZYPfz
KW94WUAz7IdZIOIcuG1zLIR3kUNUc/vdSmW120dwkIleB6pl4cVT5nDewBFJCzTS
W6jGaWaryzl7xS3ZToKTHpVeQr3avN7H+Om9TfsccY7gBV3IOIauTg9xIpDjIg52
fUcfyPq+Bhw0cWkDUQIDAQAB
-----END PUBLIC KEY-----";

            //Llave Crypto Privada del Comercio
            $llaveComercioCryptoPriv = "-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQChkt0P012gPFuAs+pT1ZPv9Iet0t5jkCtyJnpfoRF/rpsBvNnP
ecULTfFgksBIfNRk5iSCKwvS2x2YWeSAyWAnrRBWal+bHTP7Ghpm9JyMeCBtqE7+
Wky89SUlIhVL0G8822a9IMKuSrQLZKQHUdvoBWKGxiimNtS0xqRA2k9LkwIDAQAB
AoGALfCeHkPQ2SCQO9XsCI4vlAiJE2ljhheV4s0B+nLHkPBKboINxCoxVneA7B6N
wY6MrWuqccBfikJ29byJdfzL88hIquAw/pB1RwXnfoo51R7+iMa7LV3DhBgmHNqn
Z8k/4Oe4GkRBtb7en3h0SYSOYiniRQZv37R3MNATp0zoppECQQDTmGbDXiyjIofl
sawacGQ9kjaknCn8Rz2ATmMYFay9qmNv63MdprsFkSLpgbAidrydStEIFt6ImhIR
7ETQ5PE/AkEAw3sijgf16rJ4aLexEwtGzIQpXl9FjuPsYONUGCxD0Hjt3jXUDMgC
Ub46nxOyJhT7l/N0qwQKK1921fWDaBK8rQJAIPbjO5iWV6vvhAdUCqJr23PF84so
t1bZ6/1KTtxBlyLObwc9Xec1x74+tCYyLIxXAiI/woTdkmZ+XjBy4CBqvQJBALJI
TFSTKBKcILsIxWOkBjciVgRvCaZvczdOPXUqcdLhZ7ghCbt6crsQrrBEq1aWDnwg
GwiZz5iNIXmzx9wUMqUCQQCUn5XnaJpj/Hu/PYeaquvkVchai4buf50wqaP02U7q
27AgVQaabXSl5E4+LtfqVcn8fTxijDP4kVdDWtHcCf3E
-----END RSA PRIVATE KEY-----";

            //Parametros de Recepción de Autorización
            $arrayIn['IDACQUIRER'] = $_POST['IDACQUIRER'];
            $arrayIn['IDCOMMERCE'] = $_POST['IDCOMMERCE'];
            $arrayIn['XMLRES'] = $_POST['XMLRES'];
            $arrayIn['DIGITALSIGN'] = $_POST['DIGITALSIGN'];
            $arrayIn['SESSIONKEY'] = $_POST['SESSIONKEY'];
            $arrayOut = '';
                
            //Ejecución de Creación de Valores para la Solicitud de Interpretación de la Respuesta
            if(VPOSResponse($arrayIn,$arrayOut,$llaveVPOSFirmaPub,$llaveComercioCryptoPriv,$vector)){
                if(!empty($arrayOut['authorizationResult']) && $arrayOut['authorizationResult']=='00'){
                    $this->db->update('envios',array('tipo_pago'=>4,'status'=>'Pagado','descripcion_pago'=>$arrayOut['errorMessage']),array('process_id'=>$arrayOut['purchaseOperationNumber']));
                }
                $this->loadView(array('view'=>'procesarpago','arrayIn'=>$arrayIn,'arrayOut'=>$arrayOut));
            }else{
                throw new Exception("Error durante el proceso de interpretación de la respuesta. <br/>",503);
            }
            
        }
}
