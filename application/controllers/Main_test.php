<?php
/**************************
 * Archivo de prueba de alignet.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        date_default_timezone_set('America/Asuncion');
    }

    public function index() {
        $this->loadView('main');
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']))
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    } else
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url('panel'));
        } else
            header("Location:" . base_url('panel'));
    }

    function pages($titulo) {
        $titulo = urldecode(str_replace("-", "+", $titulo));
        if (!empty($titulo)) {
            $pagina = $this->db->get_where('paginas', array('titulo' => $titulo));
            if ($pagina->num_rows() > 0) {
                $this->loadView(array('view' => 'paginas', 'contenido' => $pagina->row()->contenido, 'title' => $titulo));
            } else
                $this->loadView('404');
        }
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    public function loadView($param = array('view' => 'main')) {
        if (is_string($param))
            $param = array('view' => $param);
        $this->load->view('template', $param);
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }

    function sendMailer() {
        $this->load->library('mailer');
        $this->mailer->mail('joncar.c@gmail.com', 'Test', 'Test');
    }

    function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
            foreach($usuario as $n=>$v){             
             $mensaje->texto = str_replace('{usuarios.'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{usuarios.'.$n.'}',$v,$mensaje->titulo);   
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }
            //correo('joncar.c@gmail.com',$mensaje->titulo,$mensaje->texto);
        }
        
        function testCorreo(){
            /*$this->enviarcorreo((object)array('email'=>'joncar.c@gmail.com','nombre'=>'Jonathan'),1);
            $this->enviarcorreo((object)array('email'=>'pedromarecos23@hotmail.com','nombre'=>'Jonathan'),1);*/
            $this->enviarcorreo((object)array('email'=>'xxx@mail-tester.com','nombre'=>'Jonathan'),1);
        }
        
        function procesar_pago(){
            $this->load->helper('alignet');
            //Componentes de Seguridad
            //Vector Hexadecimal
            $vector = "0FF00AA000000000";

            //Llave Firma Publica de Alignet
            $llaveVPOSFirmaPub = "-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCvJS8zLPeePN+fbJeIvp/jjvLW
Aedyx8UcfS1eM/a+Vv2yHTxCLy79dEIygDVE6CTKbP1eqwsxRg2Z/dI+/e14WDRs
g0QzDdjVFIuXLKJ0zIgDw6kQd1ovbqpdTn4wnnvwUCNpBASitdjpTcNTKONfXMtH
pIs4aIDXarTYJGWlyQIDAQAB
-----END PUBLIC KEY-----";

            //Llave Crypto Privada del Comercio
            $llaveComercioCryptoPriv = "-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQDFZ6v6yB2r+qfhxMSpkmBzwU5mhwF1P48CA6qmK08qApb+bpYr
K0mY1XUOa/yd1O7wm0zblbj/Ap2VM2v/mkCn2lqxLQ/nJwCw/OBijRAffgDrhRqL
VxOZo1jVs4AW7gZZAmY/CshA/x95JMmxTFFoWxvbfRpc1wVGl6L2lAEAXwIDAQAB
AoGADutD4yMmou3BrF1ro4ktgvIUE8mWQZ85FqTiS8awJ/OkJ7yB2uddm5BeVWWx
5NfH6yVBjMLnBVbURAdr3RhyVFr9mwkpZpHEvbsuu+X4PGrAOksGeFWo9QsuHnM8
TZuQ5eGRm+G7U1Hc7tEPYJ4Q2Ts8+Z5MAucSXuJZ2tYZjJkCQQD+eCP4hA799sUC
whwgpkirmNwIAb5BOjUbe3QITyb7/fTl85LIHgBfpj3YY3lLptFQCmIuwakM8pLd
4nmNyO69AkEAxpeoTPia9Jz+bs0FUIw975Ne9+opcPTNL/2MmfDa3UfWkjbz0+Q9
xT4r2EBMSs+8nUbfbFfqrOh6HDoYciu7SwJAVuPbFH6zR9C9UMXLKPLemYlmiG1f
85HWlH3BDx1H6H+84dAb7K0h34c6UusUfdqGWlhRUjnaaYtiztUIQHu92QJBAIv+
ZtbzvZBIh5emEWQJqLOVSLItMM9+0pJNA2uVfUPWG21xLYsmb2D3BcNo+B8YuBaK
2n8urrD7JPF3BrqdF2cCQBQ3bQ+qp4mQpQfi+KfHQwuPtGOj5UycitaxpGOiFRfN
s+05dBqAZjJBvYK/5/vYzcyDagfUB0lIMCIRlqoaqWg=
-----END RSA PRIVATE KEY-----";

            //Parametros de Recepción de Autorización
            $arrayIn['IDACQUIRER'] = $_POST['IDACQUIRER'];
            $arrayIn['IDCOMMERCE'] = $_POST['IDCOMMERCE'];
            $arrayIn['XMLRES'] = $_POST['XMLRES'];
            $arrayIn['DIGITALSIGN'] = $_POST['DIGITALSIGN'];
            $arrayIn['SESSIONKEY'] = $_POST['SESSIONKEY'];
            $arrayOut = '';
                
            //Ejecución de Creación de Valores para la Solicitud de Interpretación de la Respuesta
            if(VPOSResponse($arrayIn,$arrayOut,$llaveVPOSFirmaPub,$llaveComercioCryptoPriv,$vector)){
                if(!empty($arrayOut['authorizationResult']) && $arrayOut['authorizationResult']=='00'){
                    $this->db->update('envios',array('tipo_pago'=>4,'status'=>'Pagado','descripcion_pago'=>$arrayOut['errorMessage']),array('process_id'=>$arrayOut['purchaseOperationNumber']));
                }
                $this->loadView(array('view'=>'procesarpago','arrayIn'=>$arrayIn,'arrayOut'=>$arrayOut));
            }else{
                throw new Exception("Error durante el proceso de interpretación de la respuesta. <br/>",503);
            }
            
        }
}
