<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-sm-offset-4">
            <form action="<?= base_url('registro/forget') ?>" method="post" onsubmit="return validar(this)" role="form" class="form-horizontal">
                <?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?>
                <?= !empty($msj)?$msj:'' ?>
                <?= input('email','Email','email') ?>        
                <div align='center'><button type="submit" class="btn btn-success">Recuperar contraseña</button></div>
            </form>
        </div>        
    </div>
</div>