<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
        <title><?= empty($title) ? 'MVDEOCORREO' : $title ?></title> <!-- fonts --> 
        <link href="favicon.png" type="image/x-icon" rel="shortcut icon">
        <link href="<?= base_url() ?>assets/css/master.css" rel="stylesheet">
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.3.min.js"></script>
        <script src="<?= base_url() ?>assets/js/modernizr.custom.js"></script>
        <?php if(!empty($scripts)): ?>
            <?= $scripts ?>
        <?php endif ?>
    </head>
    <body>
        <!-- Loader -->
        <!--<div id="page-preloader"><span class="spinner"></span></div>-->
        <!-- Loader end -->
        <div class="layout-theme animated-css" id="wrapper" data-header="sticky" data-header-top="200">
            <?php $this->load->view('includes/template/header'); ?>
            <?php $this->load->view($view); ?>
            <?php $this->load->view('includes/template/footer'); ?>
        </div>
        <?php $this->load->view('includes/template/scripts'); ?>
    </body>
</html>


