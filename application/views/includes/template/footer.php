<footer class="footer wow">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="footer__section">
                    <div class="footer__logo">
                        <img class="img-responsive" src="<?= base_url() ?>assets/img/logo3.png" height="19" width="148" alt="logo">
                    </div>
                    <div class="footer__info">
                        <p>Mollis semper lobortis vitae phasellus turpis commodo libero vamus sed dolor do nec turpis. Praesent sit amet non magna vel diam trum elementum. </p>
                        <p>Suspendisse congue sapien vestib massa posuere lacinia.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <ul class="social-links list-inline">
                            <li><a target="_blank" href="<?= $this->db->get('ajustes')->row()->facebook ?>"><i class="ic social_facebook"></i></a></li>
                            <li><a target="_blank" href="<?= $this->db->get('ajustes')->row()->twitter ?>"><i class="ic social_twitter"></i></a></li>
                            <li><a target="_blank" href="<?= $this->db->get('ajustes')->row()->youtube ?>"><i class="ic social_youtube"></i></a></li>
                            <li><a target="_blank" href="<?= $this->db->get('ajustes')->row()->instagram ?>"><i class="ic social_instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <section class="footer__section">
                    <h3 class="footer__title">PAGINAS</h3>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <ul class="footer__list">
                                <li> <a href="<?= site_url() ?>"> Inicio</a></li>
                                <li> <a href="<?= site_url('p/quienes-somos') ?>"> Quienes Somos</a></li>
                                <li> <a href="<?= site_url('p/oficinas') ?>"> Nuestras Oficinas</a></li>
                                <li> <a href="<?= site_url('p/servicios') ?>"> Servicios</a></li>                                
                            </ul>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <ul class="footer__list">
                                <li> <a href="<?= site_url('p/tarifas') ?>"> Tarifas</a></li>
                                <li> <a href="<?= site_url('p/aduana') ?>"> Aduana</a></li>
                                <li> <a href="<?= site_url('p/contactenos') ?>"> Contactenos</a></li>
                            </ul>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <section class="footer__section">
                    <h3 class="footer__title">INFORMACION DE CONTACTO</h3>
                    <ul class="list-contacts">
                        <li class="list-contacts__item"><i class="icon icon_pin"></i><?= $this->db->get('ajustes')->row()->direccion_contacto ?></li>
                        <li class="list-contacts__item"><i class="icon icon_phone"></i><?= $this->db->get('ajustes')->row()->telefono ?></li>
                        <li class="list-contacts__item"><i class="icon icon_mail"></i><a class="list-contacts__link" href="mailto:<?= $this->db->get('ajustes')->row()->correo ?>"><?= $this->db->get('ajustes')->row()->correo ?></a></li>
                        <li class="list-contacts__item"><i class="icon icon_clock"></i><?= $this->db->get('ajustes')->row()->horario ?></li>
                    </ul>
                </section>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="copyright">MVDEOCORREOS © 2016 All rights reserved.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>