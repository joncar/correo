<div class="main-slider slider-pro" id="my-slider"
     data-slider-width="1920"
     data-slider-height="1045">

    <div class="sp-slides">
        
        <!-- Slide 1 -->
        <div class="sp-slide">
            <img class="sp-image img-responsive" src="" height="580" width="1600" alt="slider">

            <h1 class="main-slider__title sp-layer"
                data-horizontal="13vw"
                data-vertical="35%"
                data-show-transition="up"
                data-hide-transition="left"
                data-show-duration="800"
                data-show-delay="400"
                data-hide-delay="400">
                <?= $b->titulo ?>
            </h1>

            <div class="main-slider__text sp-layer hidden-xs "
                 data-horizontal="13vw"
                 data-vertical="50%"
                 data-height="30%"
                 data-width="35%"
                 data-show-transition="up"
                 data-hide-transition="left"
                 data-show-duration="800"
                 data-show-delay="1200"
                 data-hide-delay="1200">
                <p><?= $b->subtitulo ?></p>
                <div class="border-decor border-decor_mod-a"></div>
            </div>
        </div>
        <!-- end sp-slide -->
        
    </div>
</div>