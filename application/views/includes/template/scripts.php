<!-- end layout-theme -->
<!-- SCRIPTS MAIN -->
<script src="<?= base_url() ?>assets/js/jquery-migrate-1.2.1.js"></script>
<script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/waypoints.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<?= base_url() ?>assets/js/modernizr.custom.js"></script>
<script src="<?= base_url() ?>assets/js/cssua.min.js"></script>
<!--SCRIPTS THEME-->
<!-- Home slider -->
<script src="<?= base_url() ?>assets/plugins/slider-pro/dist/js/jquery.sliderPro.js"></script>
<!-- Sliders -->
<script src="<?= base_url() ?>assets/plugins/owl-carousel/owl.carousel.min.js"></script>
<!-- Isotope -->
<script src="<?= base_url() ?>assets/plugins/isotope/isotope.pkgd.min.js"></script>
<!-- Modal -->
<script src="<?= base_url() ?>assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script>
<!-- Chart -->
<script src="<?= base_url() ?>assets/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<!-- Animation -->
<script src="<?= base_url() ?>assets/plugins/scrollreveal/dist/scrollreveal.min.js"></script>
<!-- Flickr-->
<script src="<?= base_url() ?>assets/plugins/jflickrfeed/jflickrfeed.min.js" ></script>
<!-- Menu for android-->
<script src="<?= base_url() ?>assets/js/doubletaptogo.js" ></script>
<!-- Custom -->
<script src="<?= base_url() ?>assets/js/custom.js"></script>