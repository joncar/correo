<!-- HEADER -->
<header class="header">
    <div class="top-header navbar-header">
        <div class="container">
            <div class="row">
                <div class="top-header__slogan col-md-6"></div>

                <div class="col-md-6">
                    <div class="top-header__info">
                        <ul class="social-links list-inline">
                            <li><a target="_blank" href=""><i class="ic social_facebook"></i></a></li>
                            <li><a target="_blank" href=""><i class="ic social_twitter"></i></a></li>
                            <li><a target="_blank" href=""><i class="ic social_youtube"></i></a></li>
                            <li><a target="_blank" href=""><i class="ic social_instagram"></i></a></li>
                        </ul>
                    </div>
                    <div class="header-language btn-group">
                        <button type="button" class="header-language__btn dropdown-toggle" data-toggle="dropdown">ESP <span class="caret"></span></button>
                        <ul class="language-list dropdown-menu list-unstyled" role="menu">
                            <li class="language-list__item"><a class="header-language__link" href="#">ENG</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /top-header -->

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-12"> 
                <a href="home-1.html" class="logo"> 
                    <img src="<?= base_url() ?>assets/img/logo.png" width="150" height="auto" alt="logo"></a> 
            </div>
            <div class="col-md-9 col-xs-12">
                <div class="header-contacts">
                    <div class="header-contacts__item">
                        <div class="header-contacts__name">Correo</div>
                        <div class="header-contacts__info"><a class="header-contacts__link" href="mailto:"></a></div>
                    </div>
                    <div class="header-contacts__item">
                        <div class="header-contacts__name">Teléfono</div>
                        <div class="header-contacts__info"></div>
                    </div>
                    <div class="header-contacts__item">
                        <div class="header-contacts__name">Horario</div>
                        <div class="header-contacts__info"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-navig">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <div class="navbar yamm">
                        <!--<a class="btn_header_search visible-xs" href="#"><i class="icon icon_search"></i></a>-->
                        <div class="search-form-modal transition">
                            <form class="navbar-form header_search_form">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Search">
                                </div>
                                <button class="btn_search btn btn-primary btn-effect">Search</button>
                                <i class="fa fa-times search-form_close"></i>
                            </form>
                        </div>
                        <div class="navbar-header visible-xs">
                            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                        </div>

                        <nav id="navbar-collapse-1" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="<?= site_url() ?>" >Inicio</a></li>
                                <li><a href="<?= site_url('p/empresa') ?>" >Empresa</a>
                                    <ul role="menu" class="dropdown-menu">
                                        <li> <a href="<?= site_url('p/quienes-somos') ?>" >Quienes Somos</a> </li>
                                        <li> <a href="<?= site_url('p/oficinas') ?>" >Nuestras Oficinas</a> </li>
                                    </ul>
                                </li>
                                <li><a href="<?= site_url('p/servicios') ?>l">Servicios</a></li>
                                <li><a href="<?= site_url('p/tarifas') ?>">Tarifas</a></li>
                                <li><a href="<?= site_url('p/aduana') ?>">Aduana</a></li>
                                <li><a href="<?= site_url('p/contactenos') ?>">Contactenos</a></li>                                
                                <?php if(!empty($_SESSION['user'])): ?>
                                <li><a href="<?= base_url('panel') ?>">Mis envios</a></li>
                                <li><a href="<?= base_url('seguridad/perfil/edit/'.$this->user->id) ?>">Perfil</a></li>
                                <li><a href="<?= base_url('main/unlog') ?>">Salir</a></li>
                                <?php else: ?>
                                <li class="active"><a href="<?= base_url('panel') ?>">Registrarse</a></li>
                                <?php endif ?>
                            </ul>

                            <!--<a class="btn_header_search hidden-xs" href="#"><i class="icon icon_search"></i></a>-->
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </div><!-- /navig -->
</header><!-- /header -->