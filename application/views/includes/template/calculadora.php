  <div class="form-group">
    <label for="exampleInputEmail1">Peso en gramos</label>
    <input type="number" class="form-control" id="peso" placeholder="0">
  </div>    
  <div class="form-group">
    <label for="exampleInputEmail1">Total a pagar</label>
    <input type="text" class="form-control" id="precio" placeholder="0" readonly="">
  </div>  
<script>
    $(document).on('ready',function(){
        $("#peso").change(function(){
            if(!isNaN($(this).val())){
                var peso = parseFloat($(this).val());
                var precio = 0;
                if(peso<=999){
                    precio = 16;
                }else if(peso>=1000 && peso<=4990){
                    precio = 15;                    
                }
                else if(peso>=5000 && peso<=19990){
                    precio = 14;
                }
                else if(peso>=20000 && peso<=40000){
                    precio = 13;
                }
                else{
                    precio = 'Consultar';
                }
                if(!isNaN(precio) && precio<16){
                    peso = peso/1000;
                    precio*= peso;
                }
                $("#precio").val(precio);
            }
        });
    });
</script>