<?php $this->load->view('includes/template/banner'); ?>
<div class="section-banner section-bg-2 wow">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner" id="banner01">
                    <div class="banner__description">Es muy sencillo, te lo describimos en pocos pasos...</div>
                    <div class="banner__btn-wrap">
                        <a href="/" class="btn btn-default btn-effect">AMPLIAR</a>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /section-banner -->
<div class="section-area wow">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-default border-top">
                    <div class="carousel_mod-b owl-carousel owl-theme enable-owl-carousel"
                         data-min480="1"
                         data-min768="2"
                         data-min992="2"
                         data-min1200="3"
                         data-pagination="false"
                         data-navigation="true"
                         data-auto-play="4000"
                         data-stop-on-hover="true">

                        <article class="post post_mod-a clearfix">
                            <div class="entry-media">
                                <a href="<?= base_url() ?>assets/media/posts/370x220/1.jpg" class="prettyPhoto">
                                    <img class="img-responsive" src="<?= base_url('images/crea-direccion.jpg') ?>" width="370" height="220" alt="Foto">
                                </a>
                            </div>
                            <div class="entry-main">
                                <div class="entry-header clearfix">
                                    <i class="icon icon_question_alt2"></i>
                                    <div class="entry-header__wrap">
                                        <h3 class="entry-title ui-title-inner"><a href="blog-details.html">PORQUE COMPRAR EN PARAGUAY</a></h3>
                                        <div class="border-decor border-decor_mod-b"></div>
                                    </div>
                                </div>
                                <div class="entry-content">
                                    <p>Vivamus elementum laoreet lorem. Maecenas er felis sed mollis semper lobortis vitae phasellus commodo libero Vivamus sed dolor. Quisque portitor leo as vitae tincidun rutrum urna turpis nullam consequat.</p>
                                </div>
                                <a class="link" href="blog-details.html">LEER MAS</a>
                            </div>
                        </article>

                        <article class="post post_mod-a clearfix">
                            <div class="entry-media">
                                <a href="<?= base_url() ?>assets/media/posts/370x220/2.jpg" class="prettyPhoto">
                                    <img class="img-responsive" src="<?= base_url('images/porque-comprar.jpg') ?>" width="370" height="220" alt="Foto">
                                </a>
                            </div>
                            <div class="entry-main">
                                <div class="entry-header clearfix">
                                    <i class="icon icon_contacts_alt"></i>
                                    <div class="entry-header__wrap">
                                        <h3 class="entry-title ui-title-inner"><a href="blog-details.html">CREA TU DIRECCION</a></h3>
                                        <div class="border-decor border-decor_mod-b"></div>
                                    </div>
                                </div>
                                <div class="entry-content">
                                    <p>Vivamus elementum laoreet lorem. Maecenas er felis sed mollis semper lobortis vitae phasellus commodo libero Vivamus sed dolor. Quisque portitor leo as vitae tincidun rutrum urna turpis nullam consequat.</p>
                                </div>
                            </div>
                        </article>

                        <article class="post post_mod-a clearfix">
                            <div class="entry-media">
                                <a href="<?= base_url() ?>assets/media/posts/370x220/3.jpg" class="prettyPhoto">
                                    <img class="img-responsive" src="<?= base_url('images/calculadora.jpg') ?>" height="220" width="370" alt="Foto">
                                </a>
                            </div>
                            <div class="entry-main">
                                <div class="entry-header clearfix">
                                    <i class="icon icon_document_alt"></i>
                                    <div class="entry-header__wrap">
                                        <h3 class="entry-title ui-title-inner"><a href="blog-details.html">Calculadora de envío</a></h3>
                                        <div class="border-decor border-decor_mod-b"></div>
                                    </div>
                                </div>
                                <div class="entry-content">
                                    <?= $this->load->view('includes/template/calculadora') ?>
                                </div>
                            </div>
                        </article>

                    </div><!-- /carousel -->
                </div>
            </div>
        </div>
    </div>
</div><!-- /section-area -->