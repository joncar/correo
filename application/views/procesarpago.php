<div class="container well">      
        <table>
            <tr><td>Resultado de la Transacción</td><td><?php echo !empty($arrayOut['authorizationResult'])?$arrayOut['authorizationResult']:'';?></td></tr>
            <tr><td>Detalle del Resultado</td><td><?php echo !empty($arrayOut['errorCode'])?$arrayOut['errorCode']." - ".$arrayOut['errorMessage']:'';?></td></tr>            
            <tr><td>Número de Tarjeta</td><td><?php echo !empty($arrayOut['cardNumber'])?$arrayOut['cardNumber']:'';?></td></tr>
            <tr><td>Número de Operacion</td><td><?php echo !empty($arrayOut['purchaseOperationNumber'])?$arrayOut['purchaseOperationNumber']:'';?></td></tr>
            <?php 
                $monto = !empty($arrayOut['purchaseAmount'])?$arrayOut['purchaseAmount']/100:0;
                $monto .= !strpos($monto,'.')?'.00':'';
            ?>
            <tr><td>Monto</td><td><?php echo $monto;?></td></tr>
            <!--Ejemplo recepción de campos reservados en parametro reserved1-->
            <tr><td>Reservado 1</td><td><?php echo !empty($arrayOut['reserved1'])?$arrayOut['reserved1']:'';?></td></tr>
            <!--Ejemplo recepción de campos reservados de la inclusión financiera-->
            <tr><td>Reservado 13</td><td><?php echo !empty($arrayOut['reserved13'])?$arrayOut['reserved13']:'';?></td></tr>
            <tr><td>Reservado 14</td><td><?php echo !empty($arrayOut['reserved14'])?$arrayOut['reserved14']:'';?></td></tr>
            <tr><td>Reservado 15</td><td><?php echo !empty($arrayOut['reserved15'])?$arrayOut['reserved15']:'';?></td></tr>
            <tr><td>Reservado 16</td><td><?php echo !empty($arrayOut['reserved16'])?$arrayOut['reserved16']:'';?></td></tr>
            <tr><td>Reservado 17</td><td><?php echo !empty($arrayOut['reserved17'])?$arrayOut['reserved17']:'';?></td></tr>
	</table>   
</div>