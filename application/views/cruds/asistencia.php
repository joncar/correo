<?= $output ?>
<script>
    $(document).ready(function() {
        $(document).on('change','#field-materias_plan_id',function(e) {
                e.stopPropagation();
                var selectedValue = $('#field-materias_plan_id').val();					
                $.post('ajax_extension/programacion_materias_plan_id/materias_plan_id/'+encodeURI(selectedValue.replace(/\//g,'_agsl_')), {anho_lectivo:$("#field-anho_lectivo").val()}, function(data) {					
                var $el = $('#field-programacion_materias_plan_id');
                          var newOptions = data;
                          $el.empty(); // remove old options
                          $el.append($('<option></option>').attr('value', '').text(''));
                          $.each(newOptions, function(key, value) {
                            $el.append($('<option></option>')
                               .attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));
                            });
                          //$el.attr('selectedIndex', '-1');
                          $el.chosen().trigger('liszt:updated');

        },'json');
        $('#field-programacion_materias_plan_id').change();
        });
    });
    
    $(document).ready(function() {
            $(document).on('change','#field-modalidades_id',function(e) {
                if($(this).val()!=''){
                        e.stopPropagation();
                        var selectedValue = $('#field-modalidades_id').val();					
                        $.post('ajax_extension/programacion_carreras_id/sedes_id/'+encodeURI(selectedValue.replace(/\//g,'_agsl_')), {sedes_id:$("#field-sedes_id").val(),'modalidades_id':$("#field-modalidades_id").val()}, function(data) {
                        var $el = $('#field-programacion_carreras_id');
                                  var newOptions = data;
                                  $el.empty(); // remove old options
                                  $el.append($('<option></option>').attr('value', '').text(''));
                                  $.each(newOptions, function(key, value) {
                                    $el.append($('<option></option>')
                                       .attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));
                                    });
                                  //$el.attr('selectedIndex', '-1');
                                  $el.chosen().trigger('liszt:updated');

                },'json');
                $('#field-programacion_carreras_id').change();
            }
            });
    });
                        
    $('#crudForm').on('change','#field-programacion_materias_plan_id',function(){
        if($(this).val()!=''){
            <?php $return = isset($edit)?'/1':'/false'; ?>
            $.post('<?= base_url('procesosacademicos/asistencia_alumno_detalle_field/1/1/1').$return ?>',{programacion_materias_plan_id:$(this).val()},function(data){
                $("#asistencia_field_box").html('<label id="asistencia_display_as_box" for="field-asistencia"> Asistencia : </label>'+data);
                $(".multiselect").multiselect();
            });
        }
    });
</script>